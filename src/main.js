import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import './plugins/element.js'

// github上的树形结构表格
import TreeTable from 'vue-table-with-tree-grid'

// 导入富文本编辑及其样式
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

// 导入vuex（没用到）
// import store from './store'

// 导入字体图标
import './assets/fonts/iconfont.css'
// 导入全局样式表
import './assets/css/global.css'
// 导入axios，并挂载到原型对象上，那么所有vc和vm都能访问到axios了
import axios from 'axios'
// 配置请求的根路径
axios.defaults.baseURL = 'https://lianghj.top:8888/api/private/v1/'
// 为每一个请求挂载拦截器
axios.interceptors.request.use(config => {
  // config就是请求对象，有个headers属性，然后给这个headers添加Authorization字段
  config.headers.Authorization = window.sessionStorage.getItem("token")
  // 在最后必须return config 固定写法 
  return config
},
  error => {
    // 对请求错误做些什么
    return Promise.reject(error);
  })


Vue.prototype.$http = axios;
Vue.component('tree-table', TreeTable)
// 将富文本编辑器注册为全局可用的组件
Vue.use(VueQuillEditor)


Vue.config.productionTip = false


// 定义时间的全局过滤器
// 后端传入的一般都是毫秒数
Vue.filter('dateFormat', function (originVal) {
  const dt = new Date(originVal)

  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate() + '').padStart(2, '0')
  const hh = (dt.getHours() + '').padStart(2, '0')
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  const ss = (dt.getSeconds() + '').padStart(2, '0')

  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
